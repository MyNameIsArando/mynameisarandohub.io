# mynameisarando.github.io
### How To Not Code a Website

All code is free to use, edit to your hearts content.

This Repo includes -

- [My Site](http://arando.club/)
- [Start Page](http://arando.club/other/start)
- [Site Archive](http://arando.club/other/old)
- [Index](http://arando.club/other/index)

### Many Thanks to

- [Pace.js](https://github.hubspot.com/pace/docs/welcome/) - Loading bar
- [Van D](https://codepen.io/vaaandiep/pen/bpbbgN) - Loading wrapper
- [cferdinandi](https://github.com/cferdinandi/smooth-scroll) - Smooth scrolling
- [Rob Gravelle](https://codepen.io/blackjacques/pen/LLQKKJ) - Music progress bar